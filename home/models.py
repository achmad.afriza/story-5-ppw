from django.db import models

# Create your models here.
class Friend(models.Model):
    name = models.CharField(primary_key=True, max_length=30)
    hobby = models.CharField(max_length=20)
    favorite = models.CharField(max_length=20)
    year = models.ForeignKey("home.ClassYear", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name}"

class ClassYear(models.Model):
    year = models.CharField(primary_key=True, max_length=4)
    yearname = models.CharField(max_length=10)

    def __str__(self):
        return f"{self.year}"
    
    @property
    def getName(self):
        return f"{self.yearname}"