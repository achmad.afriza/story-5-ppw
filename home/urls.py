from django.urls import path, include
from django.conf.urls import url
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('friends/', views.friends_list, name='friends'),
    path('friends/new/', views.friends_new, name='friends_new'),
    path('delete/', views.delete_friend, name='delete_friend'),
]