from django import forms
from .models import Friend, ClassYear

class POSTFriend(forms.ModelForm):
    class Meta:
        model = Friend
        fields = (
            'name',
            'hobby',
            'favorite',
            'year',
        )
    
    new_year = forms.CharField(max_length=4, required=False, label="New Year")
    new_yearname = forms.CharField(max_length=10, required=False, label="New Year Name")

    def __init__(self, *args, **kwargs):
        super(POSTFriend, self).__init__(*args, **kwargs)
        self.fields['year'].required = False

    def clean(self):
        year = self.cleaned_data.get('year')
        new_year = self.cleaned_data.get('new_year')
        new_yearname = self.cleaned_data.get('new_yearname')

        if not year and not new_year and not new_yearname:
            raise forms.ValidationError("Must specify either Year or New Year!")
        elif not year:
            year, created = ClassYear.objects.get_or_create(year=new_year, yearname=new_yearname)
            self.cleaned_data['year'] = year
        
        return super(POSTFriend, self).clean()


# class POSTClassYear(forms.ModelForm):
#     class Meta:
#         model = ClassYear
#         fields = (
#             'year',
#             'yearname',
#         )