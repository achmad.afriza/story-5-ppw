from django.shortcuts import render, redirect
from .forms import POSTFriend
from .models import Friend, ClassYear
from django.contrib import messages
from django.core import validators
from django.urls import reverse

# Create your views here.
def index(request):
    context = {
        'nav': [
            ['#profile', 'About Me'],
            ['#life', 'Experiences'],
            ['/friends/', 'My Friends'],
            ['#footer', 'Contact Me']
        ],
    }

    return render(request, 'home.html', context)

def friends_list(request):
    friends = Friend.objects.all().order_by('name')

    context = {
        'friends': friends,
        'nav': [
            [reverse('home:index'), 'About Me'],
            ['/friends/new/', 'Add Friends'],
            [reverse('home:delete_friend'), 'Delete Friends'],
        ],
    }

    return render(request, 'friends.html', context)

def friends_new(request):    
    if request.method == 'POST':
        friend_form = POSTFriend(request.POST)
        # class_form = POSTClassYear(request.POST)

        if friend_form.is_valid():
            friend_form.save()
            return redirect('/friends/')
        # elif class_form.is_valid():
        #     class_form.save()
        #     return redirect('friends_new')
        else:
            messages.error(request, "Please correct the errors.")
    else:
        friend_form = POSTFriend()
        # class_form = POSTClassYear()

    context = {
        'friend_form': friend_form,
        # 'class_form': class_form,
        'nav': [
            [reverse('home:index'), 'About Me'],
            ['/friends/', 'My Friends'],
            [reverse('home:delete_friend'), 'Delete Friends'],
        ],
    }

    return render(request, 'friendforms.html', context)

def delete_friend(request):
    friend = Friend.objects.all().order_by('name')
    if request.method == 'GET' and request.GET.get('delete_name'):
        delete_friend = Friend.objects.get(name=request.GET.get('delete_name'))
        delete_friend.delete()
        return redirect(reverse('home:delete_friend'))
    
    context = {
        'friends': friend,
        'nav': [
            [reverse('home:index'), 'About Me'],
            ['/friends/', 'My Friends'],
        ],
    }

    return render(request, 'deletefriends.html', context)